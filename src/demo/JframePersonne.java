package demo;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;
import services.MyConnection;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.border.LineBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.BevelBorder;

public class JframePersonne extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textNum;
	private JTextField textNom;
	private JTextField textPrenom;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JframePersonne frame = new JframePersonne();
					frame.setVisible(true);
					frame.fillData();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JframePersonne() {
		setTitle("CRUD Operations");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 911, 625);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 230, 140));
		contentPane.setForeground(new Color(240, 230, 140));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection c = MyConnection.getConnection();				
				try {
					String sql = "DELETE FROM personne WHERE num='" + textNum.getText() + "'";
					PreparedStatement ps;
					ps = c.prepareStatement(sql);
					int row = ps.executeUpdate();
					//Reset Text Fields
					textNum.setText("");
					textPrenom.setText("");
					textNom.setText("");
					
					JOptionPane.showMessageDialog(null, "Entr�e supprim�e dans la base!");
					fillData();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnDelete.setBounds(768, 531, 89, 23);
		contentPane.add(btnDelete);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Connection c = MyConnection.getConnection();
				
				try {
					String sql = "UPDATE personne SET nom='" + textNom.getText() + "', prenom='" + textPrenom.getText() + "'WHERE num='" + textNum.getText() + "'";
					PreparedStatement ps = c.prepareStatement(sql);
					int rs = ps.executeUpdate();
					//Reset Text Fields
					textNum.setText("");
					textPrenom.setText("");
					textNom.setText("");
					
					JOptionPane.showMessageDialog(null, "Modifi� dans la base!");
					fillData();
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnUpdate.setBounds(669, 531, 89, 23);
		contentPane.add(btnUpdate);
		
//		JScrollPane scrollPane = new JScrollPane();
//		scrollPane.setBounds(35, 37, 463, 386);
//		contentPane.add(scrollPane);		
//		
//		scrollPane.setViewportView(table_3);
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Je vais chercher les informations dans les champs input que j'ai nomm�
				Connection c = MyConnection.getConnection();
				
				try {
					String sql = "INSERT INTO personne (nom,prenom) VALUES ('" + textNom.getText() + "','" + textPrenom.getText() +"')";
					PreparedStatement ps =  c.prepareStatement(sql);
					int rs = ps.executeUpdate();
					//Reset Text Fields
					textNum.setText("");
					textPrenom.setText("");
					textNom.setText("");
					
					JOptionPane.showMessageDialog(null, "Enregistr� dans la base!");
					fillData();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnInsert.setBounds(562, 531, 89, 23);
		contentPane.add(btnInsert);
		
		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection c = MyConnection.getConnection();
				try {
					String sql = "SELECT * FROM personne";
					
					PreparedStatement ps = c.prepareStatement(sql);
					ResultSet rs = ps.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnSelect.setBounds(35, 531, 89, 23);
		contentPane.add(btnSelect);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.RAISED, new Color(222, 184, 135), null, new Color(192, 192, 192), new Color(192, 192, 192)));
		scrollPane.setBounds(35, 28, 442, 369);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		textNum = new JTextField();
		textNum.setBounds(652, 47, 158, 20);
		contentPane.add(textNum);
		textNum.setColumns(10);
		
		textNom = new JTextField();
		textNom.setBounds(652, 90, 158, 20);
		contentPane.add(textNom);
		textNom.setColumns(10);
		
		textPrenom = new JTextField();
		textPrenom.setBounds(652, 137, 158, 20);
		contentPane.add(textPrenom);
		textPrenom.setColumns(10);
		
		JLabel lblNum = new JLabel("Num");
		lblNum.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNum.setBounds(578, 50, 46, 14);
		contentPane.add(lblNum);
		
		JLabel lblPrenom = new JLabel("Pr\u00E9nom");
		lblPrenom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPrenom.setBounds(578, 140, 64, 14);
		contentPane.add(lblPrenom);
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNom.setBounds(578, 93, 46, 14);
		contentPane.add(lblNom);
	}
	
	public void fillData() {
		Connection c = MyConnection.getConnection();
		try {
			String sql = "SELECT * FROM personne";
			
			PreparedStatement ps = c.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			table.setModel(DbUtils.resultSetToTableModel(rs));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
